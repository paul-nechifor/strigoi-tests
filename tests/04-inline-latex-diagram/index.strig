---
type: jade
main: true
async:
  content: innerDoc
---
doctype html
html
  body
    != strigoi.async.content

---
type: latex
id: tree
---
\documentclass{article}

\usepackage{tikz}
% Opțiuni la diagrame tikz.
\usetikzlibrary{patterns, trees, matrix, arrows, decorations.pathmorphing, decorations.pathreplacing}
\begin{document}
\pagestyle{empty}
\begin{tikzpicture}[level distance=1.0cm]]
    \tikzstyle{level 1}    = [sibling distance=6.4cm]
    \tikzstyle{level 2}    = [sibling distance=3.2cm]
    \tikzstyle{level 3}    = [sibling distance=1.6cm]
    \tikzstyle{level 4}    = [sibling distance=0.8cm]
    \tikzstyle{every node} = [font=\tiny]
    \tikzstyle{parinte}    = [circle, draw, inner sep=0pt, minimum size=4pt, fill=black]
    \tikzstyle{frunza}     = [circle, draw, inner sep=0pt, minimum size=18pt]

    \node[parinte]{}
        child[sibling angle=10] {node[parinte]{}
            child {node[parinte]{}
                child {node[parinte]{}
                    child {node[frunza]{0000}}
                    child {node[frunza]{0001}}
                }
                child[missing]{}
            }
            child {node[parinte]{}
                child {node[parinte]{}
                    child[missing]{}
                    child {node[frunza]{0101}}
                }
                child {node[parinte]{}
                    child {node[frunza]{0110}}
                    child {node[frunza, very thick]{0111}}
                }
            }
        }
        child {node[parinte]{}
            child {node[parinte]{}
                child {node[parinte]{}
                    child {node[frunza]{1000}}
                    child[missing]{}
                }
                child {node[parinte]{}
                    child {node[frunza]{1010}}
                    child[missing]{}
                }
            }
            child {node[parinte]{}
                child {node[parinte]{}
                    child {node[frunza]{1100}}
                    child[missing]{}
                }
                child {node[parinte]{}
                    child {node[frunza]{1110}}
                    child {node[frunza]{1111}}
                }
            }
        };

    % Desenarea acoladelor.
    \tikzstyle{acoltext} = [midway, yshift=-14pt]
    \tikzstyle{acol} = [decorate, decoration={brace, amplitude=4pt, mirror, raise=2pt}]
    \def\desubt{-4.5}
    \def\marime{0.70}
    \def\La{-1.55}
    \def\Lb{-3.15}
    \def\Lc{-6.35}
    \def\Ld{ 0.05}
    \draw[acol] (\La,\desubt) -- (\La+1*\marime+0.0,\desubt) node[acoltext]{\footnotesize $L_0$};
    \draw[acol] (\Lb,\desubt) -- (\Lb+2*\marime+0.1,\desubt) node[acoltext]{\footnotesize $L_1$};
    \draw[acol] (\Lc,\desubt) -- (\Lc+4*\marime+0.3,\desubt) node[acoltext]{\footnotesize $L_2$};
    \draw[acol] (\Ld,\desubt) -- (\Ld+8*\marime+0.7,\desubt) node[acoltext]{\footnotesize $L_3$};
\end{tikzpicture}
\end{document}
